package com.example.prmabe.services;

import com.example.prmabe.entity.UserModel;
import com.example.prmabe.repositories.UserRepo;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
@AllArgsConstructor
public class UserService {

    private UserRepo userRepo;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public UserModel editUser(UserModel user, String email) {
        UserModel loggedUser = userRepo.findByEmail(email);
        if (loggedUser.getId().equals(user.getId())) {
            user.setPassword(loggedUser.getPassword());
            userRepo.save(user);
            return user;
        } else {
            throw new ResponseStatusException(HttpStatus.CONFLICT, "You dont have privilege to edit this user.");
        }
    }

    public UserModel registerUser(UserModel user) {
        if (userRepo.existsByEmail(user.getEmail())) throw new ResponseStatusException(HttpStatus.CONFLICT, "User with this email is already registered");
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userRepo.insert(user);
        user.setPassword(null);
        return user;
    }

    /**
     * Returns users whole profile except password
     * @param email logged user email
     * @return UserModel
     */
    public UserModel getUsersDetails(String email) {
        UserModel user = userRepo.findByEmail(email);
        if (user == null) {
            throw new UsernameNotFoundException("User not found");
        }
        user.setPassword(null);
        return user;
    }
}
