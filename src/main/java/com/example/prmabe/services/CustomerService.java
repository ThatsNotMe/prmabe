package com.example.prmabe.services;

import com.example.prmabe.entity.Customer;
import com.example.prmabe.entity.UserModel;
import com.example.prmabe.repositories.CustomerRepo;
import com.example.prmabe.repositories.InvoiceRepo;
import com.example.prmabe.repositories.UserRepo;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class CustomerService {


    private CustomerRepo customerRepo;
    private InvoiceRepo invoiceRepo;
    private UserRepo userRepo;

    public Customer saveCustomer(Customer customer, String email) {
        UserModel loggedUser = userRepo.findByEmail(email);
        customer.setUserId(loggedUser.getId());
        customerRepo.insert(customer);
        return customer;
    }

    /**
     * @param id    id of customer to delete
     * @param email logged users email
     */
    public void deleteCustomer(String id, String email) {
        UserModel loggedUser = userRepo.findByEmail(email);
        Optional<Customer> customer = customerRepo.findById(id);
        if (customer.isEmpty()) throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Customer not found");
        if (!loggedUser.getId().equals(customer.get().getUserId())) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, "This is not your customer!");
        }
        if(invoiceRepo.existsByCustomerId(id)) throw new ResponseStatusException(HttpStatus.CONFLICT, "This customer has atleast one Invoice");
        customerRepo.deleteById(id);
    }

    /**
     * @param id    id of customer
     * @param email logged user email
     * @return Customer
     */
    public Customer findCustomerById(String id, String email) {
        UserModel loggedUser = userRepo.findByEmail(email);
        Optional<Customer> customer = customerRepo.findById(id);
        if (customer.isEmpty()) throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Customer not found");

        if (!loggedUser.getId().equals(customer.get().getUserId())) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, "This is not your customer!");
        }

        return customer.get();
    }

    /**
     * @param email logged user email
     * @return List<Customer>
     */
    public List<Customer> findAllUsersCustomers(String email) {
        UserModel user = userRepo.findByEmail(email);
        return customerRepo.findAllByUserId(user.getId());
    }

    public Customer editCustomer(Customer customer, String email) {
        UserModel loggedUser = userRepo.findByEmail(email);
        if (!isMyCustomer(customer.getId(), email)) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, "This is not your customer!");
        }
        customer.setUserId(loggedUser.getId());
        customerRepo.save(customer);
        return customer;
    }

    public Boolean isMyCustomer(String customerId, String email) {
        UserModel loggedUser = userRepo.findByEmail(email);
        return  customerRepo.existsByIdAndUserId(customerId, loggedUser.getId());

    }
}
