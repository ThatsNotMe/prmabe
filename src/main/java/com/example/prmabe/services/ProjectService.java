package com.example.prmabe.services;

import com.example.prmabe.entity.Customer;
import com.example.prmabe.entity.Project;
import com.example.prmabe.entity.UserModel;
import com.example.prmabe.repositories.InvoiceRepo;
import com.example.prmabe.repositories.ProjectRepo;
import com.example.prmabe.repositories.UserRepo;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ProjectService {
    private ProjectRepo projectRepo;
    private InvoiceRepo invoiceRepo;
    private UserRepo userRepo;

    public void saveProject(Project project, String email) {
        UserModel loggedUser = userRepo.findByEmail(email);
        project.setUserId(loggedUser.getId());
        projectRepo.insert(project);
    }

    /**
     * @param id    id of customer to delete
     * @param email logged users email
     */
    public void deleteProject(String id, String email) {
        UserModel loggedUser = userRepo.findByEmail(email);
        Optional<Project> project = projectRepo.findById(id);
        if(project.isEmpty())
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Project not found.");

        if (!loggedUser.getId().equals(project.get().getUserId()))
            throw new ResponseStatusException(HttpStatus.CONFLICT, "This is not your project!");

        projectRepo.deleteById(id);
    }

    public List<Project> findAllUserProjects(String email) {
        UserModel user = userRepo.findByEmail(email);
        return projectRepo.findAllByUserId(user.getId());
    }

    public Project editProject(Project project, String email) {
        UserModel loggedUser = userRepo.findByEmail(email);
        if (!projectRepo.existsByUserId(loggedUser.getId())) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, "This is not your project!");
        }
        project.setUserId(loggedUser.getId());
       projectRepo.save(project);
        return project;
    }
}
