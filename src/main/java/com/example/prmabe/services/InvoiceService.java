package com.example.prmabe.services;

import com.example.prmabe.entity.Invoice;
import com.example.prmabe.entity.InvoiceItem;
import com.example.prmabe.entity.UserModel;
import com.example.prmabe.repositories.CustomerRepo;
import com.example.prmabe.repositories.InvoiceRepo;
import com.example.prmabe.repositories.UserRepo;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
@AllArgsConstructor
public class InvoiceService {

    private InvoiceRepo invoiceRepo;
    private CustomerRepo customerRepo;
    private CustomerService customerService;
    private UserRepo userRepo;

    public void saveInvoice(Invoice invoice, String email) {
        UserModel loggedUser = userRepo.findByEmail(email);

        if (!customerRepo.existsByIdAndUserId(invoice.getCustomerId(), loggedUser.getId()))
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Customer was not found.");

        invoice.setUserId(loggedUser.getId());

        double finalPrice = 0.0d;
        for (InvoiceItem item : invoice.getInvoiceItems()) {
            double price = item.getNumberOfUnits() * item.getPricePerUnit();
            item.setPrice(price);
            finalPrice += price;
        }
        invoice.setPriceTotal(finalPrice);

        invoiceRepo.insert(invoice);
    }

    public void deleteInvoiceById(String id, String email) {
        UserModel loggedUser = userRepo.findByEmail(email);
        Invoice invoice = invoiceRepo.findById(id).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Invoice does not exists!"));

        if (!invoice.getUserId().equals(loggedUser.getId()))
            throw new ResponseStatusException(HttpStatus.CONFLICT, "You are trying delete another users invoice!");

        invoiceRepo.deleteById(id);
    }

    public List<Invoice> getAllUsersInvoices(String email) {
        UserModel loggedUser = userRepo.findByEmail(email);
        return invoiceRepo.findAllByUserId(loggedUser.getId());
    }

    public Invoice editInvoice(Invoice invoice, String email){
        UserModel loggedUser = userRepo.findByEmail(email);

        if (!customerService.isMyCustomer(invoice.getCustomerId(), email)) throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Customer does not exists.");;
        double finalPrice = 0.0d;
        for (InvoiceItem item : invoice.getInvoiceItems()) {
            double price = item.getNumberOfUnits() * item.getPricePerUnit();
            item.setPrice(price);
            finalPrice += price;
        }
        invoice.setPriceTotal(finalPrice);
        invoice.setUserId(loggedUser.getId());

        invoiceRepo.save(invoice);
        return  invoice;
    }

}
