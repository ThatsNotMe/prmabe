package com.example.prmabe.validation;

import com.example.prmabe.entity.Customer;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;

@Component
public class CombinedNotNullValidator implements ConstraintValidator<CombinedNotNull, Customer> {
    String[] fields;

    @Override
    public void initialize(final CombinedNotNull combinedNotNull) {
        fields = combinedNotNull.fields();
    }

    @Override
    public boolean isValid(Customer value, ConstraintValidatorContext context) {
        final BeanWrapperImpl beanWrapper = new BeanWrapperImpl(value);
        ArrayList<Object> fieldValues = new ArrayList<>();
        for (final String f : fields) {
            fieldValues.add(beanWrapper.getPropertyValue(f));
        }
        if ((fieldValues.get(2) == null || fieldValues.get(2) == "") && (fieldValues.get(0) == null || fieldValues.get(0) == "")) return false;
        if (fieldValues.get(0) != null && fieldValues.get(0) != "") {
            return fieldValues.get(1) != null && fieldValues.get(1) != "";
        } else {
            return fieldValues.get(1) == null || fieldValues.get(1) == "";
        }
    }
}
