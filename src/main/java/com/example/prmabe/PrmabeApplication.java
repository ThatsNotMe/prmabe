package com.example.prmabe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrmabeApplication {

	public static void main(String[] args) {
		SpringApplication.run(PrmabeApplication.class, args);
	}

}
