package com.example.prmabe.controller;

import com.example.prmabe.entity.Invoice;
import com.example.prmabe.services.InvoiceService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/invoice")
@AllArgsConstructor
public class InvoiceController {

    private InvoiceService invoiceService;

    @PostMapping
    private ResponseEntity<Invoice> create(@Valid @RequestBody Invoice invoice, Principal principal) {
        invoiceService.saveInvoice(invoice, principal.getName());
        return new ResponseEntity<>(invoice, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    private ResponseEntity<String> deleteById(@PathVariable("id") String id, Principal principal) {
        invoiceService.deleteInvoiceById(id, principal.getName());
        return new ResponseEntity<>("Invoice with id: " + id + " was deleted.", HttpStatus.OK);
    }

    @GetMapping
    private ResponseEntity<List<Invoice>> getInvoices(Principal principal) {
        return new ResponseEntity<>(invoiceService.getAllUsersInvoices(principal.getName()), HttpStatus.OK);
    }

    @PutMapping
    private ResponseEntity<Invoice> editInvoice(@Valid @RequestBody Invoice invoice, Principal principal) {
        Invoice updatedInvoice = invoiceService.editInvoice(invoice, principal.getName());
        return new ResponseEntity<>(updatedInvoice, HttpStatus.OK);
    }

}
