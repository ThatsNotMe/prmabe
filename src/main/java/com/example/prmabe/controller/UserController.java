package com.example.prmabe.controller;

import com.example.prmabe.entity.UserModel;
import com.example.prmabe.services.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@RequestMapping("/api/users")
@AllArgsConstructor
public class UserController {

    private UserService userService;



    @PostMapping("/signup")
    public ResponseEntity<UserModel> signUp(@RequestBody UserModel user) {
        UserModel registeredUser = userService.registerUser(user);
        return new ResponseEntity<>(registeredUser, HttpStatus.OK);
    }

    @GetMapping("/me")
    public ResponseEntity<UserModel> getInfoAboutLoggedUser(Principal principal) {
        UserModel user = userService.getUsersDetails(principal.getName());
        return new ResponseEntity<>(user, HttpStatus.OK);

    }

    @PostMapping("/edit")
    private ResponseEntity<UserModel> editUser(@RequestBody UserModel user, Principal principal) {
        UserModel updatedUser = userService.editUser(user, principal.getName());
        return new ResponseEntity<>(updatedUser, HttpStatus.OK);
    }


}
