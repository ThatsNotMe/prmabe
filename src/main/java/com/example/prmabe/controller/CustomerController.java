package com.example.prmabe.controller;

import com.example.prmabe.entity.Customer;
import com.example.prmabe.services.CustomerService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/customers")
@AllArgsConstructor
public class CustomerController {

    private CustomerService customerService;


    @PostMapping
    private ResponseEntity<Customer> addCustomer(@Valid @RequestBody Customer customer, Principal principal) {
        Customer savedCustomer = customerService.saveCustomer(customer, principal.getName());
        return new ResponseEntity<>(savedCustomer, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    private ResponseEntity<String> deleteById(@PathVariable("id") String id, Principal principal) {
        customerService.deleteCustomer(id, principal.getName());
        return new ResponseEntity<>("Customer with id: " + id + " was deleted.", HttpStatus.OK);
    }

    @GetMapping("/{id}")
    private ResponseEntity<Customer> getOneCustomerById(@PathVariable String id, Principal principal) {
        Customer customer = customerService.findCustomerById(id, principal.getName());
        return new ResponseEntity<>(customer, HttpStatus.OK);
    }

    @GetMapping
    private ResponseEntity<List<Customer>> getCustomers(Principal principal) {
        List<Customer> customers = customerService.findAllUsersCustomers(principal.getName());
        return new ResponseEntity<>(customers, HttpStatus.OK);
    }

    @PutMapping
    private ResponseEntity<Customer> editCustomer(@Valid @RequestBody Customer customer, Principal principal) {
        return new ResponseEntity<>(customerService.editCustomer(customer, principal.getName()), HttpStatus.OK);
    }

}
