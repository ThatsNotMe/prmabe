package com.example.prmabe.controller;

import com.example.prmabe.entity.Project;
import com.example.prmabe.entity.UserModel;
import com.example.prmabe.repositories.ProjectRepo;
import com.example.prmabe.repositories.UserRepo;
import com.example.prmabe.services.ProjectService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/projects")
@AllArgsConstructor
public class ProjectController {

    private ProjectService projectService;

    @PostMapping
    private ResponseEntity<Project> create( @Valid @RequestBody Project project, Principal principal) {
        projectService.saveProject(project, principal.getName());
        return new ResponseEntity<>(project, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    private ResponseEntity<String> deleteById(@PathVariable("id") String id, Principal principal) {
        projectService.deleteProject(id, principal.getName());
        return new ResponseEntity<>("Project with id: " + id + " was deleted.", HttpStatus.OK);
    }

    @PutMapping
    private ResponseEntity<Project> editProject(@RequestBody @Valid Project project, Principal principal) {
        return new ResponseEntity<>(projectService.editProject(project,principal.getName()),HttpStatus.OK);
    }

    @GetMapping
    private List<Project> getAllProjects(Principal principal) {
        return projectService.findAllUserProjects(principal.getName());
    }
}
