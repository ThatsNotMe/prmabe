package com.example.prmabe.repositories;

import com.example.prmabe.entity.Customer;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface CustomerRepo extends MongoRepository<Customer, String> {
    List<Customer> findAllByUserId(String id);
    void deleteByIdAndUserId (String id, String userId);
    boolean existsByIdAndUserId(String id, String userId);
    Customer findByIdAndUserId(String id, String userId);
    boolean existsById(String id);
}
