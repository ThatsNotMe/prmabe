package com.example.prmabe.repositories;

import com.example.prmabe.entity.Project;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface ProjectRepo extends MongoRepository<Project, String> {
    List<Project> findAllByUserId(String id);
    boolean existsByUserId(String id);
}
