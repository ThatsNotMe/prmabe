package com.example.prmabe.repositories;

import com.example.prmabe.entity.Invoice;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface InvoiceRepo extends MongoRepository<Invoice, String> {
    List<Invoice> findAllByUserId(String id);
    boolean existsByCustomerId(String customerId);
}
