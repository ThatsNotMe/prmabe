package com.example.prmabe.repositories;

import com.example.prmabe.entity.Address;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface AddressRepo extends MongoRepository<Address, String> {
}
