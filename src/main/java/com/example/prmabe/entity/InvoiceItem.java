package com.example.prmabe.entity;

import lombok.Data;

public @Data
class InvoiceItem {
    private String unitsOfMeasure;
    private double numberOfUnits;
    private String description;
    private double pricePerUnit;
    private double price;
}
