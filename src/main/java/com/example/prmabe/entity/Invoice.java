package com.example.prmabe.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;


public @Data
class Invoice {
    @Id
    private String id;
    @NotNull
    @Min(0)
    private int invoiceNumber;
    @NotNull
    private Date issueDate;
    private ArrayList<InvoiceItem> invoiceItems = new ArrayList<>();
    @NotBlank
    private String paymentMethod;
    @NotBlank
    private String status;
    @NotBlank
    private String customerId;
    private String userId;
    private double priceTotal;
}
