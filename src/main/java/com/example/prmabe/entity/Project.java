package com.example.prmabe.entity;

import lombok.Data;
import lombok.NonNull;
import org.springframework.data.annotation.Id;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public @Data
class Project {
    @Id
    private String id;

    @NotBlank
    private String title;
    private String desc;
    @NotBlank
    private String status;
    @Min(0)
    private float estimatedPrice;
    @Min(0)
    private float finalPrice;
    @Min(0)
    private int estimatedHours;
    @Min(0)
    private int hours;
    @Min(0)
    private int hourRate;
    @Min(0)
    private int numberOfDev;
    private String userId;
}
