package com.example.prmabe.entity;

import com.example.prmabe.validation.CombinedNotNull;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import javax.validation.constraints.NotNull;

@CombinedNotNull(fields = { "name", "surname", "companyName" })
@Document
public @Data
class Customer {

    @Id
    private String id;

    private String companyName;
    private String name;
    private String surname;
    private String ico;
    private String dic;
    @NotNull(message = "Address is required.")
    private Address address;
    private String userId;
}
