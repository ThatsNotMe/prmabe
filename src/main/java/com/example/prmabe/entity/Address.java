package com.example.prmabe.entity;

import lombok.Data;

public @Data
class Address {
    private String city;
    private String street;
    private String houseNumber;
    private String zip;
}
