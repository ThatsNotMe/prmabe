package com.example.prmabe.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import net.minidev.json.annotate.JsonIgnore;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Document
@NoArgsConstructor
public @Data
class UserModel {
    @Id
    private String id;
    private String name;
    private String surname;
    private String ico;

    private String password;
    private Address address;
    private String email;
    private String bankAccount;

}
