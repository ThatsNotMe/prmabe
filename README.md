# API documentation

>http://localhost:8080

## Project
***

**Create new project**
>-POST /api/projects/create

data:
- title: String
- desc: String
- status: String
- estimatedPrice: float
- hours:
- estimatedHours:
- hourRate:
- numberOfDev: int

***
**Delete project**
> -GET /api/projects/delete/{id}

params:
- id: String

***

## Invoices
***
**Create invoice**
> POST /api/invoices/create